package ReadExcelData;


import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ReadExcel {
public static void main(String[] args) throws Exception 
{
File src = new File("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\src\\testData\\TestDataWorkbook.xlsx");	
FileInputStream fis = new FileInputStream(src);

XSSFWorkbook wb = new XSSFWorkbook(fis);

//HSSFWorkbook - this is if the excel sheet is in xls format

//Below will go to sheet 1 in the excel workbook
XSSFSheet sheet1 = wb.getSheetAt(0);

/* As per Below
 * Getrow(0) means first row of that sheet
 * getCell(0) means first column
 *  so we are saying go to first row and first column and get string cell value from there
 */

String data0 = sheet1.getRow(0).getCell(0).getStringCellValue();

System.out.println("The data in the sheet is "+data0);


/* As per Below
 * Getrow(0) means first row of that sheet
 * getCell(1) means second column
 *  so we are saying go to first row and second column and get string cell value from there
 */

String data1 = sheet1.getRow(0).getCell(1).getStringCellValue();

System.out.println("The data in the sheet is "+data1);


/* As per Below
 * Getrow(1) means Second row of that sheet
 * getCell(0) means First column
 *  so we are saying go to Second row and First column and get string cell value from there
 */

String data2 = sheet1.getRow(1).getCell(0).getStringCellValue();

System.out.println("The data in the sheet is "+data2);


/* As per Below
 * Getrow(1) means Second row of that sheet
 * getCell(1) means Second column
 *  so we are saying go to Second row and Second column and get string cell value from there
 */
String data3 = sheet1.getRow(1).getCell(1).getStringCellValue();

System.out.println("The data in the sheet is "+data3);

wb.close();


}

}
	
