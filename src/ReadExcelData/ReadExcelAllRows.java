package ReadExcelData;


import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ReadExcelAllRows {
public static void main(String[] args) throws Exception 
{
File src = new File("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\src\\testData\\TestDataWorkbook.xlsx");	
FileInputStream fis = new FileInputStream(src);

XSSFWorkbook wb = new XSSFWorkbook(fis);

//HSSFWorkbook - this is if the excel sheet is in xls format

//Below will go to sheet 1 in the excel workbook
XSSFSheet sheet1 = wb.getSheetAt(0);

int rowcount = sheet1.getLastRowNum();

System.out.println("Total rows is "+rowcount);

for(int i=0;i<rowcount; i++)
{
	String data0=sheet1.getRow(i).getCell(0).getStringCellValue();
	System.out.println("Test data from row"+i+" is "+data0);

}

wb.close();
}

}
	
