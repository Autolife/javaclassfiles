package javainterviewQuestions;

public class unicode_gettingExecuted {

	public static void main(String[] args) 
	{
		//This is comment and below is unicode
		//\u000d System.out.println("Comment executed");
		
		//The above line executes because Java compiler parses the
		//unicode character as new line
		
	}


}
