package javainterviewQuestions;

import java.util.Scanner;

/*Step 1 : Create java.util.Scanner object to take input from the user.

Scanner sc = new Scanner(System.in)

Step 2 : Take inputString from the user.

String inputString = sc.nextLine()

Step 3 : Call replaceAll() method by passing [AEIOUaeiou] as pattern and �� as replacement string.

inputString.replaceAll(�[AEIOUaeiou]�, ��)

Step 4 : Print newInputString.*/


public class removeAllOvelsfromString {

	public static void main(String[] args) {
		System.out.println("Remove all vowels from string");
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the string");
		String str = scan.nextLine();
		String newInputString= str.replaceAll("[AEIOUaeiou]", "");
		System.out.println(newInputString);
		scan.close();
	}

}

