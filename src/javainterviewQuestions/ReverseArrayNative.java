package javainterviewQuestions;
//
import java.util.Arrays;

public class ReverseArrayNative {

	public static void main(String[] args) throws Exception
	{

		System.out.println("This is for revering a list of numbers using non collection approach");
		int temp[]={1,2,3,4,5,6,7,8,9};
		System.out.println("Array without reverse "+Arrays.toString(temp));
		reverseArray(temp);
						
	}

	public static void reverseArray(int[] data) 
	{
	for(int left=0, right=data.length -1; left<right; left++, right--)
	{
		int temp=data[left];
		data[left]=data[right];
		data[right]=temp;
	}
		System.out.println("Reverse Array");
		for(int val:data)
			System.out.println(""+val);
	}
}
