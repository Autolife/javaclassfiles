package firstlesson;

public class TimBuchannaJavaoperator1 
{
public static void main(String[] args) {
	int result = 1+2;
	System.out.println("The result is 1 + 2 = "+result);
	int previousResult = result;
	result = result-1;
	System.out.println(previousResult + " -1 = "+result);
	previousResult=result;
	result = result * 10;
	System.out.println(previousResult+ "*10 = " +result);
	previousResult=result;
	result = result / 2;
	System.out.println(previousResult+ "/2 = " +result);
	
	previousResult=result;
	result = result % 3;
	System.out.println(previousResult+ "%2 = " +result);

	previousResult=result;
	result=result + 1;
	System.out.println("Result is now " +result);
	result++; //implies result = result + 1;
	System.out.println("Result is now " +result);
	result--; //implies result = result - 1;
	System.out.println("Result is now " +result);
	result +=2;
	System.out.println("Result is now " +result);
	
}
}
