package WriteExcelData;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class WriteExcel {
public static void main(String[] args) throws Exception 
{
File src = new File("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\src\\testData\\TestDataWorkbook.xlsx");	
FileInputStream fis = new FileInputStream(src);

XSSFWorkbook wb = new XSSFWorkbook(fis);

//HSSFWorkbook - this is if the excel sheet is in xls format

//Below will go to sheet 1 in the excel workbook
XSSFSheet sheet1 = wb.getSheetAt(0);

//below will write values back to cell in sheet 
sheet1.getRow(0).createCell(2).setCellValue("Pass");
sheet1.getRow(1).createCell(2).setCellValue("Pass2");
FileOutputStream fout = new FileOutputStream(src);
wb.write(fout);

wb.close();


}

}
	
