package SeleniuminterviewQuestions;

/*Note: 0 and 1 are not prime numbers. The 2 is the only even prime number because all the other even numbers can be divided by 2.*/

//Below program will check if the number is prime or not

public class Primenumber_check {
	public static void main(String[] args) {
		System.out.println("We are in primenumber function");
		int i, m = 0, flag = 0;
		int n = 31; // this number needs to be checked
		m = n / 2;
		for (i = 2; i <= m; i++) {
			if (n % i == 0) {
				System.out.println("Number is not prime");
				flag = 1;
				break;
			}
		}
		if(flag==0)
		{
			System.out.println("Number is prime");
		}
	}
	
}
