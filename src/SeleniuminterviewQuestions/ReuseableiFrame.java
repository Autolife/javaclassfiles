package SeleniuminterviewQuestions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ReuseableiFrame {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(
				"https://www.o2.co.uk/shop/tariff/oneplus/5/?productId=1dd5c8f9-c487-4d77-8d74-ad36b605f203&planId=&contractType=paymonthly");
		driver.findElement(By.xpath("//*[@id='callToAction'][1]")).click();
		driver.findElement(By.xpath("//*[@id='qa-proceed-to-basket-dock-header']")).click();

		int number=findframenumber(driver, By.xpath("//*[@id='recaptcha-anchor']/div[5]"));
		driver.switchTo().frame(number);
		driver.findElement(By.xpath("//*[@id='recaptcha-anchor']/div[5]")).click();
		int number2=findframenumber(driver, By.xpath("//*[@id='recaptcha-verify-button']"));
		driver.switchTo().frame(number2);
		driver.findElement(By.xpath("//*[@id='recaptcha-verify-button']")).click();
		System.out.println("Method compelted - Main");
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		
		driver.close();
		driver.quit();
	}

	

	public static int findframenumber(WebDriver driver, By by)
	{
		int i;
		System.out.println("We are in switchtoFrame method");
		int frameSize=driver.findElements(By.tagName("iframe")).size();
		for (i=0;i<frameSize;i++)
		{
			driver.switchTo().frame(i);
			int count=driver.findElements(by).size();
			if(count>0)
			{
				driver.findElement(by).click();
				break;
			}
			else
			{
				System.out.println("Continue looping");
			}
		}
		
		return i;
	}
}
