package SeleniuminterviewQuestions;

/*Note: 0 and 1 are not prime numbers. The 2 is the only even prime number because all the other even numbers can be divided by 2.*/

//Below program will check if the number is prime or not

public class Primenumber_printAllPrime {
	public static void main(String[] args) {

		int num = 50;
		int count = 0;

		for (int i = 2; i <= num; i++) {

			count = 0;

			for (int j = 2; j <= i / 2; j++) {

				if (i % j == 0) {
					count++;
					break;
				}

			}

			if (count == 0) {

				System.out.println(i);

			}

		}

	}
}