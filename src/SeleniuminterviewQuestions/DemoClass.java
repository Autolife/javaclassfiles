package SeleniuminterviewQuestions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DemoClass {
		private static By userName = By.name("username");
		private static By pwd = By.id("password");
		private static By signInBtn = By.id("signInButton");
		public static WebDriver driver;
	 public static void main(String[] args) throws Exception {
		System. setProperty("webdriver.chrome.driver", "C:\\Users\\subbaiv1\\Downloads\\venkat_needed\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://o2.co.uk");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[@class='signInLink']")).click();
		DemoClass.waitTillVisible(driver, userName);
		DemoClass.setTextField(driver, userName, "abc");
		DemoClass.setTextField(driver, pwd, "abc");
		DemoClass.clickWebElement(driver, signInBtn);
		System.out.println("Successfylly login the Applicatipon");
		DemoClass.browserClose(driver);
		
	}
	public static void waitTillVisible(WebDriver driver, By locater){
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locater));
		
	}
	
	public static void setTextField(WebDriver driver, By locater, String input){
		driver.findElement(locater).clear();
		driver.findElement(locater).sendKeys(input);		
		driver.findElement(locater).sendKeys(Keys.TAB);
		
	}
	
	public static void clickWebElement(WebDriver driver, By locater) throws InterruptedException{		
		
		/*JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", locater);*/
		Thread.sleep(300);
		driver.findElement(locater).click();
		Thread.sleep(300);
		}
	public static void waitPageGetLoad(WebDriver driver){			
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);			
	}
	
	public static void browserClose(WebDriver driver){			
		//driver.close();	
		driver.quit();
		System.out.println("Browser Closed");
	}

}
