package SeleniuminterviewQuestions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutoSuggestExample {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(
				"https://fantasycricket.dream11.com/in/");

		//Below is explicit wait
		WebDriverWait wd=new WebDriverWait(driver, 5);
		
		driver.findElement(By.xpath("//*[@id='m_rtxtEmail1']")).sendKeys("as");
		wd.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='m_frmRegister']/div[1]/ul")));
		////*[@id='m_frmRegister']/div[1]/ul
		
		
		
		}
}