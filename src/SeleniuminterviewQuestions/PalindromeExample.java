package SeleniuminterviewQuestions;

class PalindromeExample{  
	 public static void main(String args[]){  
	  int r,sum=0,temp;    
	  int n=455;//It is the number variable to be checked for palindrome  
	  
	  temp=n;    //45 //4 //0  -- //455  //45 //4
	  while(n>0){    
	   r=n%10;  //getting remainder //4 //5 //0  -- //5 //5 //0 
	   sum=(sum*10)+r; //4 //5 //0  --//5 //5 //0
	   n=n/10;    //45 //4 //0 --//45 //4 
	  }    
	  if(temp==sum)    
	   System.out.println("palindrome number ");    
	  else    
	   System.out.println("not palindrome");    
	}  
	}  