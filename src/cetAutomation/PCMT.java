package cetAutomation;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PCMT {

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		/*System.setProperty("webdriver.gecko.driver","C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe");

		WebDriver driver = new FirefoxDriver();*/
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		/*driver.get(
				"https://www.o2.co.uk/shop/tariff/samsung/galaxy-s8/?productId=4c88c8c7-0251-4fe9-b34d-b7c3b3e1788d&planId=6718bcaf-b9c0-4aa0-96ce-e6b29a9966f0:49.99:29.00:sp-1906eea6-87ea-442e-a72f-8a55ace82d95&contractType=paymonthly");
*/
		
		driver.get("https://www.o2.co.uk/shop/tariff/lg/k8-like-new/?productId=9d6b21e3-a21d-45dd-a53f-86835d781165&planId=&contractType=paymonthly");
		// WebElement select =
		// driver.findElement(By.xpath("//*[@id='dataFilterSelect']"));

		/*
		 * ==============================================
		 * 
		 * Below is for sorting featured
		 * 
		 * ==============================================
		 */

	
			try {
	
				WebElement ele0 = driver.findElement(By.xpath("//*[@class='accessory-button-container']"));
				if (ele0 != null) 
				{
					System.out.println("selecting accessories");
					
				List<WebElement> DataContainer = driver
						.findElements(
						By.xpath("//*[@class='accessory-button-container']/input[@value='Add']"));

				for (int i = 0; i <= DataContainer.size(); i++) 
				{
					
					System.out.println(DataContainer.get(i).getText());
					System.out.println("Printed something");
					DataContainer.get(i).click();
				}
				
				}
			}
			catch (Exception e) {
				System.out.println("No accessories found in tariff and ");

			}
		
		Thread.sleep(10000);
		driver.quit();
	}
}