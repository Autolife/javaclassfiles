package cetAutomation;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SMP {

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		/*
		 * System.setProperty(
		 * "webdriver.gecko.driver","C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe"
		 * );
		 * 
		 * WebDriver driver = new FirefoxDriver();
		 */

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.get("http://sntb1-ppsmpw-12:8080/smp_jsecure/jsp/login.jsp");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("automation");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("nttdata007");
		driver.findElement(By.xpath("//input[@name='Submit']")).click();
		Thread.sleep(5000);

		// Below can also be triggered without mouse hover
		/*
		 * Actions action = new Actions(driver); WebElement ele1 =
		 * driver.findElement(By.
		 * xpath("//table[2]/tbody/tr/td/span/a[contains(text(),'Customer Service')]"
		 * )); action.moveToElement(ele1).build().perform(); ele1.click();
		 */
		driver.findElement(By.xpath("//table[2]/tbody/tr/td/span/a[contains(text(),'Customer Service')]")).click();

		driver.findElement(By.xpath("//input[@name='msisdn']")).sendKeys("07700269052");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@value='Search']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@alt='Balances']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@alt='Services']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@alt='Contacts']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@alt='Notification']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@alt='Settings']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span/a[contains(text(),'Payment')]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//img[@name='endcall']")).click();
		Thread.sleep(4000);
		driver.findElement(By.id("endCall")).click();
		Thread.sleep(4000);

		driver.close();
		driver.quit();
	}
}