package Lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataConfig 
{
	
	XSSFWorkbook wb;
	XSSFSheet sheet1;
public ExcelDataConfig(String ExcelPath)
{
	try {
		File src = new File(ExcelPath);	
		FileInputStream fis = new FileInputStream(src);

		wb = new XSSFWorkbook(fis);
	} 
	
	catch (Exception e) {
		System.out.println(e.getMessage());
	}

}

public String getData(int Sheetnumber, int Row, int Column) 
{
	
		sheet1 = wb.getSheetAt(Sheetnumber);
		String data = sheet1.getRow(Row).getCell(Column).getStringCellValue();
		return data;
	
}
}
