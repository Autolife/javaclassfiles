package seleniumfirstclass;

//This program clicks on a link, navigates back to the page and clicks the second link again.


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AllLinksCaptureandNavigate_example1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.ref.o2.co.uk/shop/sim-cards/sim-only-deals/#deviceType=phone&contractLength=P12M");

		List<WebElement> links = driver.findElements(By.xpath("//button[contains(text(),'Buy now')]")); 
        System.out.println("no of links:" +links.size());

        for(int i=0;i<links.size();i++)
        {
            if(!(links.get(i).getText().isEmpty()))
            {
            links.get(i).click();
            driver.navigate().back();
            links=driver.findElements(By.xpath("//button[contains(text(),'Buy now')]"));
            }       
        }
		driver.quit();
	}

}
