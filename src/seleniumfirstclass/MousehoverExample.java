package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;

public class MousehoverExample {

	//
	public static void main(String[] args) throws InterruptedException, FindFailed {
		System.out.println("Hello again!!!");
		// Optional, if not specified, WebDriver will search your path for
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		DesiredCapabilities handlSSLErr = DesiredCapabilities.chrome();
		handlSSLErr.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		// WebDriver driver = new ChromeDriver();
		// driver.get("https://www.o2.co.uk/shop/phones/");
		WebDriver driver = new ChromeDriver(handlSSLErr);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.o2.co.uk/shop");
		// Below will maximise the window
		driver.manage().window().maximize();
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		//WebElement ele0 = driver.findElement(By.id("pn1"));
		
		
		
		//="//div[@class='nav-consumer']/ul/li/a[contains(@href, '/shop')]")
		WebElement ele1 = driver.findElement(By.xpath("//div[@class='nav-consumer']/ul/li/a[contains(@href, '/shop')]"));
		Actions action = new Actions(driver);
		action.moveToElement(ele1).perform();
		Thread.sleep(3000);
		WebElement ele2 = driver.findElement(By.xpath("//a[contains(@href, 'https://www.o2.co.uk/shop/smart-tech/')]"));
		action.moveToElement(ele2).perform();
		Thread.sleep(2000);
		WebElement ele3 = driver.findElement(By.xpath("//a[contains(@href, 'https://www.o2.co.uk/shop/fitness-trackers/')]"));
		
		action.moveToElement(ele3).perform();
		action.click();
		ele3.click();
		Thread.sleep(15000);
		
		
		driver.close();
		driver.quit();

	}

}