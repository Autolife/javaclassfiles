package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CVOS {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Hello again!!!");

		// Optional, if not specified, WebDriver will search your path for
		// chromedriver.
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://54.77.147.84/web-ui/#/stockpots?skuSelect=SKUID&skuField=1AMFI32N");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By.id("username")).sendKeys("SupplyChainAdmin1");
		driver.findElement(By.id("password")).sendKeys("SupplyChainAdmin1");
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("stockpots")).click();
		Select dropdown=new Select(driver.findElement(By.id("skuSelect")));
		dropdown.selectByValue("SKUID");
		System.out.println("SKUID");

		driver.findElement(By.id("skuInput")).sendKeys("1AMFI32N");
		System.out.println("skuInput");

		driver.findElement(By.id("stockpot-search")).click();
		System.out.println("stockpot-search");
		
		Thread.sleep(6000);

		driver.findElement(By.id("arrivalDateB")).click();
		System.out.println("arrivalDateB");
		Thread.sleep(4000);

		driver.findElement(By.xpath("//*[@class='stockpot-threshold-border']/following::button[@class='btn btn-default btn-sm pull-right']")).click();

		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@class='stockpot-threshold-border']/following::span[contains(text(),'08')][1]")).click();
		// Let the user actually see something!
		
		Thread.sleep(5000);
		
		driver.findElement(By.id("changeArrivalDate")).click();
		
		Thread.sleep(5000);

		driver.quit();
		Thread.sleep(4000);
	}
}