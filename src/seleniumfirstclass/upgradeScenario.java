package seleniumfirstclass;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class upgradeScenario {

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		/*System.setProperty("webdriver.gecko.driver","C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe");

		WebDriver driver = new FirefoxDriver();*/
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		/*driver.get(
				"https://www.o2.co.uk/shop/tariff/samsung/galaxy-s8/?productId=4c88c8c7-0251-4fe9-b34d-b7c3b3e1788d&planId=6718bcaf-b9c0-4aa0-96ce-e6b29a9966f0:49.99:29.00:sp-1906eea6-87ea-442e-a72f-8a55ace82d95&contractType=paymonthly");
*/
		
		driver.get("https://accounts.ref.o2.co.uk/signin");
	
driver.findElement(By.id("username")).sendKeys("13ma88212553");
driver.findElement(By.id("password")).sendKeys("test123");
driver.findElement(By.id("signInButton")).click();
WebElement ele0=driver.findElement(By.xpath("//a[@manual_cm_sp='meganav_Shop-_-na-_-na']"));
WebElement ele1=driver.findElement(By.xpath("//a[@manual_cm_sp='meganav_Shop-_-Upgrades-_-na']"));
WebElement ele2=driver.findElement(By.xpath("//a[@manual_cm_sp='meganav_Shop-_-Upgrades-_-Upgrade Now']"));
Actions action = new Actions(driver);
action.moveToElement(ele0).perform();
Thread.sleep(5000);
action.moveToElement(ele1).perform();
Thread.sleep(5000);
action.moveToElement(ele2).perform();
Thread.sleep(5000);
ele2.click();

driver.findElement(By.xpath("//div[@class='ng-scope trade-in-offer']")).getText();
String text=driver.findElement(By.xpath("//div[@class='ng-scope trade-in-offer']")).getText();
if (text.contains("Trade in"))
{
	System.out.println("Working fine");
	System.out.println("The Text is "+text);
}

driver.findElement(By.xpath("//button[contains(text(),'Take this offer and upgrade')]")).click();

JavascriptExecutor js = (JavascriptExecutor) driver;

WebElement Question0 = driver
		.findElement(By.xpath("//select[@id='question0']"));
js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question0);
new Select(Question0).selectByValue("0");
Thread.sleep(8000);

// Select Second questionaire - Does your phone have any damage
WebElement Question1 = driver
		.findElement(By.xpath("//select[@id='question1']"));
js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question1);
new Select(Question1).selectByValue("0");

// Select Third questionaire - Could your phone be water damaged
WebElement Question2 = driver
		.findElement(By.xpath("//select[@id='question2']"));
js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question2);
new Select(Question2).selectByValue("0");

// Select Second questionaire - Remove icloud from device
WebElement Question3 = driver
		.findElement(By.xpath("//select[@id='question3']"));
js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question3);
new Select(Question3).selectByValue("0");

//	driver.findElement(By.id("continue-with-accurate-quote")).click();
System.out.println("Completed questionaire");

Thread.sleep(8000);

driver.findElement(By.xpath("//button[contains(text(),'Upgrade now')]")).click();

Thread.sleep(8000);

//Below is for adding a device from recommended section
System.out.println("Select a device in recommended devices section");
List<WebElement> DevicesName = driver.findElements(By.xpath("//div[@id='qa-recommendedDevicesTile']/a//div/p[@class='details']"));
WebElement SelectButton;
String c = null;
int k = 0;
List<String> Devices = new ArrayList<String>();
for (WebElement f : DevicesName) {
	Devices.add(f.getText());
	// System.out.println(Devices.add(f.getText()));
}

for (int i = 0; i < DevicesName.size(); i++) {
	if (DevicesName.get(i).getText().equals("Apple iPhone 7")) {
		
		System.out.println("Device name matches");
		k = i + 1;
		
		// System.out.println("k :" + k);
		c = "(//div[@id='qa-recommendedDevicesTile']/a//div/button)[" + k + "]";

		// System.out.println("xpath of button is" + c);
		SelectButton = driver.findElement(By.xpath(c));
		js.executeScript("arguments[0].click();", SelectButton);
	}
}
Thread.sleep(5000);



			driver.quit();
	}
}