package seleniumfirstclass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropdownSelection {

	public static void main(String[] args) 
	{

		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.o2.co.uk/shop/tariff/apple/iphone-7-plus/?productId=01190b51-c8cc-40cb-a135-53461ac9206f&contractType=paymonthly#tariff");
		driver.manage().window().maximize();
		//driver.findElement(By.xpath("//a[contains(text(),'Show more')]")).click();
		driver.findElement(By.xpath("//*[@id='dataFilterSelectSelectBoxItArrow']")).click();
		WebElement select = driver.findElement(By.xpath("//*[@id='dataFilterSelect']"));
		WebElement ele1=select.findElement(By.xpath("//a[contains(text(),'Monthly data (High to low)')]"));
		if (ele1 != null)
		{
			System.out.println("Current dropdown is Monthly data (High to low)");
			try{
				ele1.click();
				List<WebElement> outercontainer = driver.findElements(By.xpath("//*[@id='tariff-tile']"));

				  // =====================================
				 //		Below will display the Data available per month 
				 //====================================
				List<WebElement> DataContainer = outercontainer.get(0).
						findElements(By.xpath("//*[@class='col-xs-6 col-sm-3 dmt-container info-container']/ul/li[1]/h2"));
				        
				        
				        for (int i=0; i<=DataContainer.size();i++)
				        {
				            System.out.println(DataContainer.get(i).getText());
				        }
			    		     
				}
				catch (IndexOutOfBoundsException e) {
					
				}
		}

	} 
		
	}
