package seleniumfirstclass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import junit.framework.Assert;

public class sortingEcample2 
{
public static void main (String[] args) 

{
	//Below code will retrieve values of the 
	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.o2.co.uk/shop/tariff/apple/iphone-7-plus/?productId=01190b51-c8cc-40cb-a135-53461ac9206f&contractType=paymonthly#tariff");

	System.out.println("the title of tthe page is "+driver.getTitle());
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	driver.findElement(By.xpath("//*[@id='dataFilterSelectSelectBoxItArrow']")).click();


	
	//Below feature will display if the dropdowns are having expected result or not
	
	WebElement select = driver.findElement(By.xpath("//*[@id='dataFilterSelect']"));
	
	String[] expected = {"featured", "mdhl", "mdlh", "mchl", "mclh", "uchl", "uclh"};
	List<WebElement> allOptions = select.findElements(By.tagName("option"));

	// make sure you found the right number of elements
	if (expected.length != allOptions.size()) {
	    System.out.println("fail, wrong number of elements found");
	}
	// make sure that the value of every <option> element equals the expected value
	for (int i = 0; i < expected.length; i++) {
	    String optionValue = allOptions.get(i).getAttribute("value");
		//String optionValue = allOptions.get(i).getAttribute("value");
	    if (optionValue.equals(expected[i])) {
	        System.out.println("passed on: " + optionValue);
	    } else {
	        System.out.println("failed on: " + optionValue);
	    }
	}

	

	/*
	  List<WebElement> DD1 = driver.findElements(By.xpath("//*[@id='dataFilterSelectSelectBoxItContainer']"));
	 
	
	for(int i=0; i<DD1.size();i++)
	{
		WebElement Element1=DD1.get(i);
		String innerHTML=Element1.getAttribute("innerHTML");
		if(innerHTML.contentEquals("Featured"))
		{
			System.out.println("Selected dropdown is the featured ");
		}
		System.out.println("Values of the dropdown is "+innerHTML);
	}
	
	*/
	
	
	driver.quit();
	
    
    
}
}