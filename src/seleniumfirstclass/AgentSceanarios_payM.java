package seleniumfirstclass;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;

public class AgentSceanarios_payM {

	//
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws InterruptedException, FindFailed {
		System.out.println("Hello again!!!");
		// Optional, if not specified, WebDriver will search your path for
		// chromedriver.
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		DesiredCapabilities handlSSLErr = DesiredCapabilities.chrome();
		handlSSLErr.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		// WebDriver driver = new ChromeDriver();
		// driver.get("https://www.o2.co.uk/shop/phones/");
		WebDriver driver = new ChromeDriver(handlSSLErr);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(
				"https://retention7:my02u4tpa55w0rd@service-stf.uk.pri.o2.com/REFMSPAFU/agent/app/home?PartnerId=o2");
		// Below will maximise the window
		driver.manage().window().maximize();
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

		// Below is for clicking on the existing connection link
		/*
		 * driver.findElement(By.id("msisdn")).sendKeys("07521113738");
		 * driver.findElement(By.id("searchBtn")).click();
		 */

		/*
		 * System.out.println(driver.findElement(By.id("results")).getText());
		 * driver.findElement(By.xpath(".//*[@id='acquisition']/p/a")).click();
		 */

		// Below is for clicking on the new connection new customers link
		driver.findElement(By.xpath("//*[@id='acquisition']/p")).click();
		Thread.sleep(10000);

		// Below is for clicking on to the tariff tab
		driver.findElement(By.id("plansTab")).click();

		/*
		 * 
		 * //Below is for entering a value into the search field
		 * 
		 * driver.findElement(By.xpath(
		 * ".//*[@id='planTable_filter']/label/input")).sendKeys("�50");
		 * 
		 * //Below is clicking on the "+" icon next to tariffs
		 * driver.findElement(By.xpath("//table[@id='planTable']/tbody/tr/td/a")
		 * ).click();
		 * 
		 */

		// Clicking on the devices tab
		driver.findElement(By.id("devicesTab")).click();

		// selecting Galaxy S7 Edge 32GB Black
		driver.findElement(By.xpath(".//*[@id='deviceTable_filter']/label/input"))
				.sendKeys("Galaxy S7 Edge 32GB Black");
		Thread.sleep(5000);

		// Below is clicking on the "+" icon next to tariffs
		driver.findElement(By.xpath("//table[@id='deviceTable']/tbody/tr/td/a/img")).click();
		Thread.sleep(5000);


		/*
		 * Screen screen = new Screen();
		 * 
		 * Pattern image1 = new Pattern("\\SikuliImages\\PlusSymbol.PNG");
		 * 
		 * screen.click(image1);
		 */
		// Below is for clicking on to the tariff tab
		driver.findElement(By.id("plansTab")).click();
		Thread.sleep(4000);

		// screen.click(image1);
		driver.findElement(By.xpath("//*[@id='planTable']/tbody/tr[1]/td[1]")).click();

		driver.findElement(By.id("extrasTab")).click();
		Thread.sleep(3000);

		// Below is selecting a data allowance from extras tab
		driver.findElement(By.xpath("//*[@id='dataAllowances']/table/tbody/tr/td[1]/a")).click();

		// System.out.println(driver.findElement(By.xpath("//*[@id='dealBuilderContent']")).getText());

		Thread.sleep(6000);

		driver.findElement(By.id("startCheckoutFromPrivateBasketButton")).click();
		Thread.sleep(2000);

		driver.findElement(By.id("customerDetailsProceedButton")).click();
		Thread.sleep(2000);

		Select dropdown = new Select(driver.findElement(By.id("ccTitle")));
		dropdown.selectByIndex(1);
		Thread.sleep(2000);


		driver.findElement(By.id("ccFirstName")).sendKeys("vinu3");
		driver.findElement(By.id("ccLastname")).sendKeys("deep3");
		driver.findElement(By.id("ccEmail")).sendKeys("vinudeep.malalur3@o2.com");
		driver.findElement(By.id("ccDob")).sendKeys("23-04-1985");
		driver.findElement(By.id("ccContactNumber")).sendKeys("07888938333");
		driver.findElement(By.xpath("//input[@name='houseNum']")).sendKeys("flat 11");
		driver.findElement(By.xpath("//input[@name='postcode']")).sendKeys("SL1 1EL");
		driver.findElement(By.xpath("//input[@value='Find address']")).click();// ="//input[@value='Find
																				// address']")

		driver.findElement(By.xpath("//*[@id='creditCheckCurrentAddress']/div/div[2]/div[1]/div[1]/input")).click();// @FindBy(how=XPATH,
																													// using="//*[@id='creditCheckCurrentAddress']/div/div[2]/div[1]/div[1]/input")

		driver.findElement(By.id("yearsAtCurrentAddress")).sendKeys("10");// @FindBy(how=ID,
																			// using="yearsAtCurrentAddress")

		driver.findElement(By.id("monthsAtCurrentAddress")).sendKeys("10");// @FindBy(how=ID,
																			// using="monthsAtCurrentAddress")

		driver.findElement(By.id("accountName")).sendKeys("Test Accepta");

		driver.findElement(By.id("sortCode")).sendKeys("201596");

		driver.findElement(By.id("accountNumber")).sendKeys("10207136");

		driver.findElement(By.id("captureCardDetails")).click();

		String Mainwindow = driver.getWindowHandle();

		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!Mainwindow.equalsIgnoreCase(ChildWindow)) {
				// Switching to Child window
				driver.switchTo().window(ChildWindow);
				Thread.sleep(3000);
				driver.findElement(By.id("txtCardHolderName")).sendKeys("Test Accepta");// @FindBy(how=ID,
																						// using="txtCardHolderName")

				Select CardTypeDropDown = new Select(driver.findElement(By.id("ddlCardType")));// @FindBy(how=ID,
																								// using="ddlCardType")

				CardTypeDropDown.selectByIndex(3);

				driver.findElement(By.id("txtCardNumber")).sendKeys("4539791001730106");// @FindBy(how=ID,
																						// using="txtCardNumber")

				Thread.sleep(2000);
				Select CardMonthDropdown = new Select(driver.findElement(By.id("ddlMonth")));// @FindBy(how=ID,
																								// using="ddlMonth")

				CardMonthDropdown.selectByIndex(2);
				Thread.sleep(2000);
				Select CardYearDropdown = new Select(driver.findElement(By.id("ddlYr")));// @FindBy(how=ID,
																							// using="ddlYr")

				CardYearDropdown.selectByIndex(3);

				Thread.sleep(2000);

				driver.findElement(By.id("txtSecurityCode")).sendKeys("123");// @FindBy(how=ID,
																				// using="txtSecurityCode")

				Thread.sleep(2000);

				driver.findElement(By.id("btnPayNow")).click();// @FindBy(how=ID,
																// using="btnPayNow")

			}
		}
		// Switching to Parent window i.e Main Window.
		driver.switchTo().window(Mainwindow);

		driver.findElement(By.id("agreeToCreditCheck")).click();// @FindBy(how=ID,
																// using="agreeToCreditCheck")

		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@class='performCreditCheckBtn']")).click();
		//driver.findElement(By.id("creditCheckAndAgreementAndStatus")).click();// @FindBy(how=XPATH,
		Thread.sleep(8000);
																	// using="//*[@id='creditCheckAndAgreementAndStatus']/input[3]")
driver.findElement(By.id("regPassword")).sendKeys("vinudeep123");
driver.findElement(By.id("regConfirmPswd")).sendKeys("vinudeep123");

Select dropdown2 = new Select(driver.findElement(By.id("securityQuestion")));//@FindBy(how=ID, using="securityQuestion")

dropdown2.selectByIndex(2);
Thread.sleep(4000);
driver.findElement(By.id("data-qa-registerCustomerBtn")).sendKeys("Rotary");//@FindBy(how=ID, using="data-qa-registerCustomerBtn")

Thread.sleep(2000);

driver.findElement(By.xpath("//*[@class='registerCustomerBtn']")).click();
System.out.println("Completed Register customer ");

Thread.sleep(5000);

driver.findElement(By.xpath("//*[@class='payByCard']")).click();

Thread.sleep(4000);

if (driver.findElement(By.xpath("//div[@id='ValidationSumary']")).isDisplayed())
{
	System.out.println("Going to validate pay by card page displayed from mipay");
	driver.findElement(By.id("txtSecurityCode")).sendKeys("123");//@FindBy(how=ID, using="txtSecurityCode")

	Thread.sleep(2000);
	driver.findElement(By.id("btnPayNow")).click();//@FindBy(how=ID, using="btnPayNow")

}
else
{
	System.out.println("Above is not working");
}

	}

}