package seleniumfirstclass;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class urlValidation {

	public static void main(String[] args) throws MalformedURLException, URISyntaxException {
		System.out.println("Hello again!!!");
		/*
		 * System.setProperty(
		 * "webdriver.gecko.driver","C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe"
		 * );
		 * 
		 * WebDriver driver = new FirefoxDriver();
		 */

		/*
		 * System.setProperty("webdriver.chrome.driver",
		 * "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		 * WebDriver driver = new ChromeDriver();
		 * 
		 * driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 * driver.get("https://www.o2.co.uk/shop/my-offers/ipad"); String URL =
		 * driver.getCurrentUrl(); System.out.println("URL is " + URL);
		 * 
		 * System.out.println("Web url loaded ");
		 * 
		 * if (URL.contains("/iPad/")) { System.out.println("Worked fine"); }
		 */
		
//Below is correct code if you are validating url with ipad alone

		final URI uriIpad = URI.create(
				"https://ecom-ref-app00.ref.o2.co.uk:9443/upgrade/store/existing-customer-offers/?deviceCategory=ipad#sort=content.sorting.featured&page=1");
		String queryString = uriIpad.getQuery();
		//String subString = queryString.substring(queryString.);
		String subString = queryString.substring(queryString.lastIndexOf('-') + 1);
		System.out.println("EXTRACTED " + subString);
		
		
//Below is correct code if you are validating url with Tablet alone

		final URI uriTablet = URI.create(
				"https://ecom-ref-app00.ref.o2.co.uk:9443/upgrade/store/existing-customer-offers/?deviceCategory=tablet#sort=content.sorting.featured&page=1");
		String queryStringTablet = uriTablet.getQuery();
		//String subString = queryString.substring(queryString.);
		String subStringTablet = queryStringTablet.substring(queryStringTablet.lastIndexOf('-') + 1);
		System.out.println("EXTRACTED " + subStringTablet);
		
	}

}
