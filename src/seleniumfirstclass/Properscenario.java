package seleniumfirstclass;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@SuppressWarnings("unused")
public class Properscenario {


	//
	public static void main(String[] args) throws InterruptedException 
	{
System.out.println("Hello again!!!");
  // Optional, if not specified, WebDriver will search your path for chromedriver.
	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.o2.co.uk/shop/phones/");
	
	//Below will maximise the window
	driver.manage().window().maximize();
	  Thread.sleep(5000); 
	  
	  //Below is for the clicking on the PayG tab in phones page
	  driver.findElement(By.linkText("Pay As You Go")).click();
	  System.out.println("Clicked on payasyougo phone");
	  Thread.sleep(5000); 
	 // driver.findElement(By.linkText("View all products on one page")).click();

	  //Below is for the device listing page

	  driver.findElement(By.xpath("//*[@data-qa-device-model-family='Galaxy J3 2016']")).click();
	  System.out.println("Selected samsung Galaxy J3 2016 phone");
	Thread.sleep(9000);
	
	//Below is device details page
	  driver.findElement(By.xpath("//*[@id='deviceDetailsSubmit']")).click();
	  System.out.println("click on View your tariffs");
	  Thread.sleep(5000);
	  
	 //Below is tariff and extras page
	 // driver.findElement(By.xpath("//div[@id='PayAsYouGoTariffsGridTab']/ul/li[2]/div/button")).click();
	  
	 //Here below we are hovering the mouse to the location and selecting the element
	  
	 driver.findElement(By.xpath("//div[@id='PayAsYouGoTariffsGridTab']/div[1]/ul/li[1]/div/div[1]/h3[contains(text(), '500MB data')]//following::button[@id='callToAction'][1]")).click();
	  /*Actions action = new Actions(driver);
	  WebElement we = driver.findElement(By.xpath("//div[@id='PayAsYouGoTariffsGridTab']/ul/li[2]/div/button"));
	  action.moveToElement(we).build().perform();
	  we.click();*/
	  System.out.println("Selected a tariff");

	 
	  /*WebDriverWait wait = new WebDriverWait(driver, waitTime);
	  wait.until(ExpectedConditions.presenceOfElementLocated(locator));*/
	  
	 /* WebDriverWait wait = new WebDriverWait(driver, 50);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='PayAsYouGoTariffsGridTab']/ul/li[2]/div/button")));*/
	  
	 
//Below is adding for Basket in tariff and extras page
	boolean we1=driver.findElement(By.xpath("//*[@id='qa-proceed-to-basket']")).getText().contains("Go to basket");
	System.out.println("The element is "+we1);
	
	boolean we2=driver.findElement(By.xpath("html/body/div[1]/div/div/div/div[1]/div/div[4]/div[4]/div/div/div/div/div[1]/div/div[2]/input']")).getText().contains("Go to basket");
	System.out.println("The element is "+we2);

	  //Below is basket page
	  //driver.findElement(By.xpath("html/body/div[1]/div/div/div/div[1]/div[3]/div/div/div[1]/div[3]/div[1]/form/input"));

	

}
	
	  

}
