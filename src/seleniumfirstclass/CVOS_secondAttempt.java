package seleniumfirstclass;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

public class CVOS_secondAttempt {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Hello again!!!");

		// Optional, if not specified, WebDriver will search your path for
		// chromedriver.
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://54.77.147.84/web-ui/#/stockpots?skuSelect=SKUID&skuField=1AMFI32N");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.withTimeout(30,TimeUnit.SECONDS);
		wait.pollingEvery(5, TimeUnit.SECONDS);
		
		driver.findElement(By.id("username")).sendKeys("TradingAdmin");
		driver.findElement(By.id("password")).sendKeys("TradingAdmin1");
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("stockpots")).click();
		Select dropdown=new Select(driver.findElement(By.id("skuSelect")));
		dropdown.selectByValue("SKUID");
		System.out.println("SKUID");

		driver.findElement(By.id("skuInput")).sendKeys("1AMFI32N");
		System.out.println("skuInput");

		driver.findElement(By.id("stockpot-search")).click();
		System.out.println("stockpot-search");
		
		Thread.sleep(6000);
		
		driver.findElement(By.xpath(" //span[contains(text(),'All Shops')]/following::a[1]")).click();

		

		//driver.quit();
		Thread.sleep(4000);
	}
}