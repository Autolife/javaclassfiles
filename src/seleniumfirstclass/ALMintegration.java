package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jacob.com.LibraryLoader;

import atu.alm.wrapper.ALMServiceWrapper;
import atu.alm.wrapper.enums.StatusAs;
import atu.alm.wrapper.exceptions.ALMServiceException;

public class ALMintegration {
	WebDriver driver;

	@BeforeClass
	public void init() {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("jacob.dll.path", "C:\\Automation\\jacob-1.18\\jacob-1.18-x64.dll");
		LibraryLoader.loadJacobLibrary();

		 driver = new ChromeDriver();		
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
      @Test
      public void login() throws InterruptedException, ALMServiceException {
            
          /*  driver.get("https://www.o2.co.uk/shop/sim-cards/sim-only-deals/#deviceType=phone&contractLength=P12M");
        	System.out.println(driver.getTitle());

            Thread.sleep(2000);
            String Text=driver.findElement(By.cssSelector(".col-xs-12.promotion-ribbon")).getText();
    		System.out.println(Text);*/
        	
            updateResults();
            System.out.println("Done Updating Results");
      }

      public void updateResults() throws ALMServiceException {
            //Below will establish connection to ALM 
    	  ALMServiceWrapper wrapper = new ALMServiceWrapper(
                        "https://almo2uk.saas.hp.com/qcbin/");
            
            
    	     wrapper.connect("vinudeep.malalur", "July2017*", "DEFAULT_TELEFONICA_UK", "O_HS_NEW");
            wrapper.updateResult("SampleTestSetFolder\\SubTestSetFolder1",
                        "TestSet1", 601, "CFA_Accessories_Confirmation_Page_changes", StatusAs.PASSED);
            wrapper.close();
            wrapper.getServerDetails();
                  }
     
      @AfterClass
      public void close(){
            driver.quit();
      }
}