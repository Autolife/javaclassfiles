package seleniumfirstclass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import junit.framework.Assert;

public class TariffandExtrasPage {

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.get(
				"https://www.ref.o2.co.uk/shop/tariff/huawei/4g-pocket-hotspot-2017/?productId=a6f1d03f-7118-44c9-a5ab-ca007f580dba&planId=&contractType=paymonthly");
		Thread.sleep(3000);

		driver.findElement(By.xpath("//div[@class='sort-container ng-scope']"));
		WebElement ele = driver.findElement(By.xpath("//div[@class='filter-label']"));
		if (ele.isDisplayed()) {
			System.out.println("Filter label is displayed");
			Thread.sleep(4000);
			String text1 = driver.findElement(By.xpath("//div[@class='filter-label']")).getText();
			System.out.println(text1);
			if(text1.contains("Sort tariff")&&text1.contains("Filter"))
			{
				System.out.println("Working fine");
			}
			else
			{
				System.out.println("Not working fine");
			}

		}
		driver.close();

		driver.quit();

	}

}
