package seleniumfirstclass;

import static org.testng.Assert.fail;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class BasecomsRibbonandOthervaidation {

	public static void main(String[] args) throws MalformedURLException, URISyntaxException {
		System.out.println("Hello again!!!");
		/*
		 * System.setProperty(
		 * "webdriver.gecko.driver","C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe"
		 * );
		 * 
		 * WebDriver driver = new FirefoxDriver();
		 */

		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(
				"https://ecom-ref-app00.ref.o2.co.uk:9443/upgrade/store/existing-customer-offers/?deviceCategory=ipad#sort=content.sorting.featured&page=1");

		System.out.println("Web url loaded ");
		boolean str1 = driver
				.findElement(By.xpath("//*[@data-qa-device-model-family='iPad Air 2']/div[@class='ribbon-wrapper']"))
				.isDisplayed();
		System.out.println(str1);
		// Assert.assertEquals(title, "Tariff and extras");
		// driver.findElement(arg0)
//*[@data-qa-device-model-family='iPad mini 3']/div[1]/div[2]/div[1]/div[2]/div[2]/select[@id='memory']
		//WebElement capacity = driver.findElement(By.xpath("//select[@id='memory']"));
		WebElement capacity = driver.findElement(By.xpath("//*[@data-qa-device-model-family='iPad mini 3']/div[1]/div[2]/div[1]/div[2]/div[2]/select[@id='memory']"));
		// System.out.println("Capacity is Loaded "+capacity);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", capacity);
		if (capacity.isDisplayed()) {
			List<WebElement> elementCount = new Select(capacity).getOptions();
			// System.out.println("elementCount"+elementCount);
			if (elementCount.size() <= 1) {
				Assert.fail("There are no more than 1 option available for capacity dropdown");
			}
		}
		new Select(capacity).selectByVisibleText("128GB");
		String price1=driver.findElement(By.xpath("//div[@data-qa-device-model-family='iPad mini 3']/div/p[@class='costs ng-binding ng-scope']")).getText();
		System.out.println(price1);
		System.out.println("Now the second line is ..... ");
		new Select(capacity).selectByVisibleText("16GB");
		String price2=driver.findElement(By.xpath("//div[@data-qa-device-model-family='iPad mini 3']/div/p[@class='costs ng-binding ng-scope']")).getText();
		System.out.println(price2);
		
		//Assert.assertSame(price2,price1);
		
		Assert.assertNotSame(price2,price1);
		
	/*	if(price2.equals(price2))
		{
			Assert.fail("Price is same");
		}
		else
		{
			System.out.println("Prices are different");
		}
		*/
		driver.quit();

	}

}
