package seleniumfirstclass;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutoredirectionSecondAttempt {

	public static void main(String[] args) throws InterruptedException 
	{
   System.out.println("Hello again!!!");
  // Optional, if not specified, WebDriver will search your path for chromedriver.
	System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");

	WebDriver driver = new ChromeDriver();
	
	//Launch the browser and navigate to like new page
	driver.get("http://www.o2.co.uk/broadband/ipad-sim");

	
	//driver.get("https://www.o2.co.uk/shop/phones/?contractType=payasyougo");
	//Below will maximise the window
	driver.manage().window().maximize();
	  Thread.sleep(5000); 
	

	//Below will redirect url back to reference page
	String url=driver.getCurrentUrl();
	int length = url.length();//length of url
	System.out.println("the length of url is "+length);
	int numofChars=1;
	int preo2inurl = url.indexOf("o");	//this will get index of the o2 -1 , so it will be till www
	
System.out.println("the index of o is "+preo2inurl);


String refurl = ".ref."; // put newurl name here
String newUrl = url.substring(0, preo2inurl-numofChars)+refurl+url.substring(11,length);
System.out.println("The new url is "+newUrl);

//driver.get(newUrl);


//Below is the next steps
	 //driver.quit();
}
	
	  

}
