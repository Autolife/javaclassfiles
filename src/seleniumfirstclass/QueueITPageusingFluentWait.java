package seleniumfirstclass;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;

public class QueueITPageusingFluentWait {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(
				"https://www.ref.o2.co.uk/shop/tariff/samsung/galaxy-s8/?productId=4c88c8c7-0251-4fe9-b34d-b7c3b3e1788d&planId=6718bcaf-b9c0-4aa0-96ce-e6b29a9966f0:49.99:29.00&contractType=paymonthly&planCategory=normal");
		driver.findElement(By.id("qa-proceed-to-basket")).click();
		// FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);

		// *[@value='Go to checkout'][1]

		/*
		 * wait.pollingEvery(250, TimeUnit.MILLISECONDS); wait.withTimeout(2,
		 * TimeUnit.SECONDS);
		 *//*
			 * Function<WebDriver, Boolean> function = new Function<WebDriver,
			 * Boolean>() { public Boolean apply(WebDriver arg0) { WebElement
			 * element = arg0.findElement(By.name("securecheckout")); //String
			 * color = element.getCssValue("color");
			 * //System.out.println("The button text has color :" + color); if
			 * (color.equals("rgba(255, 255, 0, 1)")) { return true; } return
			 * false;
			 */
		/*
		 * } }; wait.until(function);
		 */

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(3, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {

				return driver.findElement(By.xpath("//*[@value='Go to checkout'][1]"));
			}

		});
	}
}
