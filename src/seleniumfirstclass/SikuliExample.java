package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class SikuliExample {
  
	public static void main(String[] args) throws Exception {

		Screen screen = new Screen();
		
		Pattern image1 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\gmail-Logo.PNG");
		
		Pattern image2 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\signinLink.PNG");

		Pattern image3 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\uname.PNG");

		Pattern image4 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\Password.PNG");
		
		Pattern image5 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\Signin-Button.PNG");
		
		//Pattern image6 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\Commands.txt");
		
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		  WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get("http://www.google.com");
				
			screen.click(image1);
			
			Thread.sleep(4000);
			
			screen.click(image2);
			
			Thread.sleep(4000);
			
			//screen.doubleClick(image6);
			
			Thread.sleep(4000);

			screen.type(image3, "Selenium'@'gmail.com");
			
			screen.type(image4, "password1123");
			
			screen.click(image5);
			
			
		
	}
	}