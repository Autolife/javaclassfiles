// This is not completley done, need to work on this in free time

package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;


public class ComboBoxClickDeviceDetailsPage_ThirdExample {

	public static void main(String[] args) throws InterruptedException {

		/*
		 * System.out.println("Hello again!!!");
		 * System.setProperty("webdriver.gecko.driver",
		 * "C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe");
		 * WebDriver driver = new FirefoxDriver();
		 */

		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.ref.o2.co.uk/shop/smartwatches/");
		
		/*
		driver.findElement(By.className("enhanced-sorts-link")).click();
		System.out.println("Clicked on sort tab");

		Thread.sleep(4000);
		////// * Using Mouse hover *********/////

	/*	Actions builder = new Actions(driver);
		WebElement Relevance = driver.findElement(By.xpath("//*[@ng-show='showSortByUserRating']/li/label/input[1]"));
		builder.moveToElement(Relevance);
		builder.click().build().perform();
		System.out.println("Clicked on User rating: High to low ");

		/* Clicking on the done button */
//		driver.findElement(By.xpath("(//button[@value='Done'])[2]")).click();

		/* Clicking on the first random Smart Watch */
		driver.findElement(By.xpath("//*[@data-qa-device-contract-type='nonconnected'][1]")).click();

		////// *Sorting is done ***************/////

		// Below will give status like in stock / out of stock etc
		
	

		try {
			String status = driver.findElement(By.className("status-info")).getText();
			System.out.println(status);
			if (status.contains("In Stock")) {
				WebElement element = driver
						.findElement(By.xpath("//select[@class='accessory-option ng-pristine ng-valid']"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element);
				new Select(element).selectByValue("4");

				WebElement DeviceDetailsQuantity = driver.findElement(
						By.xpath("//div[@on-dimension-select='selectQuantityDimension']/span[@role='combobox']"));
				String DeviceDetailsQuantityValue = DeviceDetailsQuantity.getText();
				System.out.println(DeviceDetailsQuantityValue);
				Assert.assertEquals("4", DeviceDetailsQuantityValue);


				driver.findElement(By.id("deviceDetailsSubmit")).click();

				Thread.sleep(3000);

				WebElement BasketQuantity = driver.findElement(By.id("accessory-quantitySelectBoxIt"));
				String BasketQuantityvalue = BasketQuantity.getText();
				Assert.assertEquals("4", BasketQuantityvalue);
				System.out.println("Values are correct");
				//moving to delivery page
				driver.findElement(By.xpath("//*[@name='securecheckout'][1]")).click();
				
			} else {
				driver.navigate().back();
			}
		} catch (Exception e) {
			
			WebElement element = driver
					.findElement(By.xpath("//select[@class='accessory-option ng-pristine ng-valid']"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element);
			new Select(element).selectByValue("4");
			
			
			WebElement DeviceDetailsQuantity = driver.findElement(
					By.xpath("//div[@on-dimension-select='selectQuantityDimension']/span[@role='combobox']"));
			Thread.sleep(2000);
			String DeviceDetailsQuantityValue = DeviceDetailsQuantity.getText();
			System.out.println(DeviceDetailsQuantityValue);
			Assert.assertEquals(DeviceDetailsQuantityValue,"4" );

			driver.findElement(By.id("deviceDetailsSubmit")).click();

			Thread.sleep(3000);
			WebElement BasketQuantity = driver.findElement(By.id("accessory-quantitySelectBoxIt"));
			String BasketQuantityvalue = BasketQuantity.getText();
			//Assert.assertEquals(DeviceDetailsQuantityValue, BasketQuantityvalue);
			Assert.assertEquals("4", BasketQuantityvalue);
			System.out.println("Values are correct");

//moving to delivery page
			driver.findElement(By.xpath("//*[@name='securecheckout'][1]")).click();

		}
		
		WebElement BasketQuantity = driver.findElement(By.className("basket-nonconnected"));
		String BasketQuantityvalue = BasketQuantity.getText();
		System.out.println("Your order contents is "+BasketQuantityvalue);
		try {
			
			if(BasketQuantityvalue.contains("Quantity: 4"))
			{
				System.out.println("Quantities are matching Working fine");
			}
			else{
				System.out.println("Not Working fine");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block

		}

		Thread.sleep(4000);
		// driver.navigate().back(); -- this will not work
		
		driver.findElement(By.xpath("//*[@title='Exit checkout']")).click();
		System.out.println("Clicked on the exit checkout");
		//WebElement BasketQuantity1 = driver.findElement(By.xpath("//*[@class='orders']/li[1]//*[@id='accessory-quantitySelectBoxIt']"));
		//String BasketQuantityvalue1 = BasketQuantity1.getText();
		
		
		Thread.sleep(3000);
		
		
		
		
		WebElement element2 = driver
				.findElement(By.id("accessory-quantity"));
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element2);
		new Select(element2).selectByValue("2");
		
		
		//driver.quit();
	}

}
