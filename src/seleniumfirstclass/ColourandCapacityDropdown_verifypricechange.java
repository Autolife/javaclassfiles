package seleniumfirstclass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ColourandCapacityDropdown_verifypricechange {

	public static void main(String[] args) {
		System.out.println("Hello again!!!");
		/*
		 * System.setProperty(
		 * "webdriver.gecko.driver","C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe"
		 * );
		 * 
		 * WebDriver driver = new FirefoxDriver();
		 */

		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(
				"https://www.o2.co.uk/shop/tariff/apple/iphone-7/?productId=52dbfc5f-1b25-4d90-959f-43e1257920f4&planId=&contractType=paymonthly");
		System.out.println("Web url loaded ");

		// get colour of accessory
		WebElement capacity = driver.findElement(By.xpath(
				"//*[@data-qa-device-model-family='iPad Air 2']/div[1]/div[2]/div[1]/div[2]/div[2]/select[@id='memory']"));

		// System.out.println("Capacity is Loaded "+capacity);

		System.out.println("Going to Verify promotion ribbon");

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", capacity);
		if (capacity.isDisplayed()) {
			List<WebElement> elementCount = new Select(capacity).getOptions();
			// System.out.println("elementCount"+elementCount);
			if (elementCount.size() <= 1) {
				Assert.fail("There are no more than 1 option available for capacity dropdown");
			}
		}
		new Select(capacity).selectByVisibleText("128GB");
		String price1 = driver
				.findElement(By
						.xpath("//div[@data-qa-device-model-family='iPad Air 2']/div/p[@class='costs ng-binding ng-scope']"))
				.getText();
		System.out.println(price1);
		System.out.println("Now the second line is ..... ");
		new Select(capacity).selectByVisibleText("16GB");
		String price2 = driver
				.findElement(By
						.xpath("//div[@data-qa-device-model-family='iPad Air 2']/div/p[@class='costs ng-binding ng-scope']"))
				.getText();
		System.out.println(price2);
		Assert.assertNotSame(price2, price1);

	}
}
