package seleniumfirstclass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import junit.framework.Assert;

public class TariffandExtrasInsuranceSelection {

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.get(
				"https://www.ref.o2.co.uk/shop/tariff/apple/ipad-pro-10.5-inch/?productId=d3e6f7c8-3875-4193-a09c-e87cca390e6a&planId=&contractType=paymonthly");
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='callToAction'][1]")).click();

		System.out.println("Selected a tariff");
		driver.findElement(By.xpath("//div[@class=' insurance-button-container '][1]")).click();

		System.out.println("Selected a insurance");
		Thread.sleep(3000);
		
if(driver.findElement(By.id("insuranceName")).isDisplayed())
	{
	System.out.println("Insurance is displayed in tariff and extras page and text is  - "+driver.findElement(By.id("insuranceName")).getText());
	};


driver.findElement(By.id("qa-proceed-to-basket-dock-header")).click();

if(driver.findElement(By.xpath("//section[@class='device-insurance']")).isDisplayed())
{
	System.out.println("Insurance is displayed in Basket page and text is  - "+driver.findElement(By.xpath("//section[@class='device-insurance']")).getText());

}


//driver.getPageSource().contains(ExpectedDelayedDeliveryMessage));

/*Expectedcontent=
Assert.assertTrue(
		"Assertion Failed: Expected Message: " + ExpectedDelayedDeliveryMessage
				+ " is not present in the page",
		driver.getPageSource().contains(ExpectedDelayedDeliveryMessage));


driver.getPageSource().contains("Insurance")
*/		System.out.println("Clicked on basket link from tariff and extras page");
		//driver.findElement(By.xpath("//input[@name='securecheckout'])[2]"));
		driver.findElement(By.name("securecheckout")).click();
		
		
		System.out.println("clicked on secure checkout");
		
		if(driver.findElement(By.xpath("//tr[@id='basket-insurance']")).isDisplayed())
		{
			System.out.println("Insurance is displayed in Delivery page and text is  - "+driver.findElement(By.xpath("//tr[@id='basket-insurance']")).getText());

		}
		driver.findElement(By.id("housenumber")).sendKeys("10");
		System.out.println("");
		driver.findElement(By.id("postcode")).sendKeys("SL1 1EL");
		System.out.println("");
		driver.findElement(By.id("postcode-submit")).click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("//a[contains(text(),'46-48, High St, Slough, Berks, SL1 1EL')]")).click();
		
		

	}

}
