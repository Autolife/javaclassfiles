package seleniumfirstclass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SortingExample1 
{
public static void main(String[] args) 

{
	
	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.o2.co.uk/shop/tariff/apple/iphone-7-plus/?productId=01190b51-c8cc-40cb-a135-53461ac9206f&contractType=paymonthly#tariff");


	//create an LinkedList instead of arraylist because it preserves insertion order

	List<WebElement> products_Webelement = new LinkedList<WebElement>();

	//store the products (web elements) into the linkedlist

	products_Webelement = driver.findElements(By.xpath("//img[contains(@id, 'product-collection-image')]"));

	//create another linked list of type string to store image title

	LinkedList<String> product_names =  new LinkedList<String>();

	//loop through all the elements of the product_webelement list get it title and store it into the product_names list

	for(int i=0;i<products_Webelement.size();i++){

	    String s = products_Webelement.get(i).getAttribute("alt");

	    product_names.add(s);

	}

	//send the list to chkalphabetical_order method to check if the elements in the list are in alphabetical order    

	boolean result = chkalphabetical_order(product_names);


	//print the result    

	System.out.println(result);

	
	
}

    public static boolean chkalphabetical_order(LinkedList<String> product_names){

    String previous = ""; // empty string

    for (final String current: product_names) {
        if (current.compareTo(previous) < 0)
            return false;
        previous = current;
    }

    return true;

    }
    
    
    
}