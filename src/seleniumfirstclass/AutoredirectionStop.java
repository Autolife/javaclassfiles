package seleniumfirstclass;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutoredirectionStop {

	public static void main(String[] args) throws InterruptedException 
	{
   System.out.println("Hello again!!!");
  // Optional, if not specified, WebDriver will search your path for chromedriver.
	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	
	//Launch the browser and navigate to like new page
	driver.get("https://www.ref.o2.co.uk/shop/");
	  Actions action = new Actions(driver);
	  WebElement we = driver.findElement(By.id("pn1"));
	  action.moveToElement(we).build().perform();
	  WebElement we1 = driver.findElement(By.xpath("//*[@id='pn1']/ul/li[6]/a"));
	  action.moveToElement(we1).build().perform();
	  Thread.sleep(3000);
	  WebElement we2 = driver.findElement(By.xpath("//*[@id='pn1']/ul/li[6]/ul/li[5]/a"));
	  action.moveToElement(we2).build().perform();
	  we2.click();

	
	//Below will maximise the window
	driver.manage().window().maximize();
	  Thread.sleep(5000); 
	
	driver.findElement(By.xpath("//div[@id='section1']/div[2]/div/div[2]/div/a[contains(text(),'sim free')]")).click();
	
	//Below will redirect url back to reference page
	String url=driver.getCurrentUrl();
	int length = url.length();//length of url
	System.out.println("the length of url is "+length);
	int numofChars=1;
	int preo2inurl = url.indexOf("o");	//this will get index of the o2 -1 , so it will be till www
	
System.out.println("the index of o is "+preo2inurl);

String refurl = ".ref."; // put newurl name here
String newUrl = url.substring(0, preo2inurl-numofChars)+refurl+url.substring(12,length);
System.out.println("The new url is "+newUrl);

driver.get(newUrl);


//Below is the next steps
	 //driver.quit();
}
	
	  

}
