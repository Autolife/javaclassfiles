package seleniumfirstclass;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

@Test
public class ScrollTestCase {
 

 public  void ScrollPageDown() throws InterruptedException  {
	 System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
  driver.manage().window().maximize();

  // Open Application
  driver.get("https://www.ref.o2.co.uk/shop/phones/#sort=content.sorting.featured&page=1");
  
  // Wait for 5 second
  Thread.sleep(2000);
 
 // This  will scroll page 400 pixel vertical
  ((JavascriptExecutor)driver).executeScript("scroll(0,6000)");
  
 }
}