package seleniumfirstclass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AllLinksDisplay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.ref.o2.co.uk/shop/sim-cards/sim-only-deals/#deviceType=phone&contractLength=P12M");

		List<WebElement> all_links_webpage = driver.findElements(By.xpath("//button[contains(text(),'Buy now')]")); 
		System.out.println("Total no of links Available: " + all_links_webpage.size());
		int k = all_links_webpage.size();
		System.out.println("List of links Available: ");
		for(int i=0;i<k;i++)
		{
		if(all_links_webpage.get(i).getAttribute("type").contains("button"))
		{
		//String link = all_links_webpage.get(i).getAttribute("button");
			String link = all_links_webpage.get(i).getText();
			System.out.println(link);
		
		}
		}
		driver.quit();
	}

}
