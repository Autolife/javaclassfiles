package seleniumfirstclass;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import com.google.common.base.Function;

public class DropdownSelection_upgrade {

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		System.out.println("Program started");
		driver.get("https://www.o2.co.uk/shop/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By
				.xpath("//*[@id='header-tool-signin']/div[2]/a[@title='Sign In'][@class='signInLink signInLinkInDesktop']"))
				.click();
		driver.findElement(By.id("username")).sendKeys("bvt2.cfu@gmail.com");
		driver.findElement(By.id("password")).sendKeys("cfubvt123");
		driver.findElement(By.id("signInButton")).click();
		driver.findElement(By.xpath("//span/a[contains(text(),'upgrade now.')]")).click();
		driver.findElement(By.id("upgradenowlink")).click();
		String newTab = null;
		String oldTab = driver.getWindowHandle();
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		Set<String> allTabs = driver.getWindowHandles();
		allTabs.remove(oldTab);
		Iterator<String> itr = allTabs.iterator();
		while (itr.hasNext()) {
			newTab = (String) itr.next();
		}
		driver.close();
		driver.switchTo().window(newTab);
		Thread.sleep(8000);
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(250, TimeUnit.MILLISECONDS);
		wait.withTimeout(2, TimeUnit.SECONDS);
		Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver arg0) {
				
				JavascriptExecutor js = (JavascriptExecutor) driver;
				WebElement notMyDevice = arg0.findElement(By.xpath("//div[@class='recycle-device not-your-device-box']/a"));
				js.executeScript("arguments[0].click();", notMyDevice);
							
				if (notMyDevice.isEnabled()) {
					return true;
				}
				return false;
			}
		};
		wait.until(function);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		/*WebElement notMyDevice = driver.findElement(By.xpath("//div[@class='recycle-device not-your-device-box']/a"));
		js.executeScript("arguments[0].click();", notMyDevice);
		Thread.sleep(4000);*/
		Thread.sleep(4000);
		WebElement Make = driver.findElement(By.xpath("//select[@class='recycle-make ng-pristine ng-valid']"));
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Make);
		new Select(Make).selectByVisibleText("Apple");
		Thread.sleep(3000);
		
		WebElement Model = driver.findElement(By.xpath("//select[@class='recycle-model ng-pristine ng-valid']"));
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Model);
		new Select(Model).selectByVisibleText("iPhone 7 32GB");
		Thread.sleep(3000);
		
		WebElement Network = driver.findElement(By.xpath("//select[@class='recycle-network ng-scope ng-pristine ng-valid']"));
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Network);
		new Select(Network).selectByVisibleText("Orange");
		
		driver.findElement(By.xpath("//button[contains(text(),'Update device')]")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("recycleCredit")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[contains(text(),'upgrade now')]")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//button[contains(text(),'Yes, get an accurate quote')]")).click();
		Thread.sleep(2000);
		
		//We need to add an get windowhandle functionality here
		
		
		// Set<String> handle= driver.getWindowHandles();//Return a set of window handle.swi
		 for (String handle : driver.getWindowHandles()) {

			    driver.switchTo().window(handle);}
		 Thread.sleep(4000);
		 System.out.println("Step 89");
		 System.out.println(driver.findElement(By.xpath("//*[@id='redeem-questionnaire']/p[@class='info']")).getText());
        
		 //Select first questionaire - Is your phone fully functional
		 WebElement Question0 = driver.findElement(By.xpath("//select[@id='question0'][@class='select-questionnaire ng-pristine ng-valid']"));
 		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question0);
 		new Select(Question0).selectByValue("0");
 		Thread.sleep(8000);

		 //Select Second questionaire - Does your phone have any damage
		WebElement Question1 = driver
				.findElement(By.xpath("//select[@id='question1'][@class='select-questionnaire ng-pristine ng-valid']"));
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question1);
		new Select(Question1).selectByValue("0");
		

		 //Select Third questionaire - Could your phone be water damaged
		WebElement Question2 = driver
				.findElement(By.xpath("//select[@id='question2'][@class='select-questionnaire ng-pristine ng-valid']"));
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question2);
		new Select(Question2).selectByValue("0");
		
		

		 //Select Second questionaire - Remove icloud from device
		WebElement Question3 = driver
				.findElement(By.xpath("//select[@id='question3'][@class='select-questionnaire ng-pristine ng-valid']"));
		js.executeScript("arguments[0].setAttribute('style', 'display:block;')", Question3);
		new Select(Question3).selectByValue("0");
		
		driver.findElement(By.id("continue-with-accurate-quote")).click();
		System.out.println("Completed questionaire");
		
		
		Thread.sleep(8000);
		driver.close();
		driver.quit();

	}
}