package seleniumfirstclass;

import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


public class SikuliAudioCapturetoText {
  
	public static void main(String[] args) throws Exception {

	  System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
	  WebDriver driver = new ChromeDriver();
	  driver.manage().window().maximize();

	 // driver = new FirefoxDriver();
 //   baseUrl = "https://www.o2.co.uk/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.get("https://www.o2.co.uk/shop/tariff/apple/iphone-7/?productId=bb990746-03f6-4fa0-872c-2dfecb03b0f9&planId=bc7c284c-df1c-42f5-bf8c-83553bff5dd9:19.99:30.00:ip-d7297698-15c4-4f16-a076-63cce4145b93&contractType=paymonthly");
    driver.findElement(By.id("qa-proceed-to-basket")).click();
	    driver.findElement(By.id("email")).clear();
    driver.findElement(By.id("email")).sendKeys("tester111@gmail.com");
    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | undefined | ]]
	driver.switchTo().frame("undefined");
    driver.findElement(By.cssSelector("div.recaptcha-checkbox-checkmark")).click();
    Thread.sleep(2000);
	String source = driver.getPageSource();
	System.out.println("The page source is "+source);
	
    //Below is for adding sikuli
	
	
	Screen screen = new Screen();
	
	Pattern image1 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\audioSymbol.PNG");
	Pattern image2 = new Pattern("C:\\Automation\\SITAUTOMATION\\workspace\\Javalearning\\SikuliImages\\PlayiconforCaptcha.PNG");
	
	screen.wait(image1,20);
	screen.click(image1);
	
	screen.wait(image2,20);
	screen.click(image2);
	
	
/*	Point coordinates =  driver.findElement(By.id("recaptcha-audio-button")).getLocation();
     Robot robot = new Robot();
	  robot.mouseMove(coordinates.getX(),coordinates.getY()+120);
	*/  
  
	}
	}
