package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class DeliveryPageValidation {
	static WebDriver driver;

	public static void main(String[] args) throws Exception {

		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.get(
				"https://www.ref.o2.co.uk/shop/tariff/apple/ipad-pro-10.5-inch/?productId=d3e6f7c8-3875-4193-a09c-e87cca390e6a&planId=&contractType=paymonthly");
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='callToAction'][1]")).click();

		System.out.println("Selected a tariff");

		driver.findElement(By.id("qa-proceed-to-basket-dock-header")).click();

		System.out.println("Clicked on basket link from tariff and extras page");

		driver.findElement(By.name("securecheckout")).click();

		System.out.println("clicked on secure checkout");

		driver.findElement(By.id("housenumber")).sendKeys("10");
		System.out.println("");
		driver.findElement(By.id("postcode")).sendKeys("SL1 1EL");
		System.out.println("");
		driver.findElement(By.id("postcode-submit")).click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("//a[contains(text(),'46-48, High St, Slough, Berks, SL1 1EL')]")).click();

		// Below is for verifying delivery page
		String ExpectedText = "";
		String ActualText = "";

		// Verifying Asterisk is present below Delivery header
		ExpectedText = "You'll need to give details for all fields marked with an asterisk (*)";
		ActualText = driver.findElement(By.xpath("//*[@id='delivery-section']/div[1]/div/div/div/p")).getText();
		//System.out.println(ActualText);

		Assert.assertTrue(ActualText.contains(ExpectedText),
				"Assertion Failed: Expected Message: " + ExpectedText + " is not present in the page");
		System.out.println("Assertion worked");

		/*driver.findElement(By.xpath(xpathExpression));
		driver.findElement(By.xpath(xpathExpression));
		driver.findElement(By.xpath(xpathExpression));*/
	}
}

