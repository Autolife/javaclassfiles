package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseCommsValidation {

	public static void main(String[] args) {
		System.out.println("Hello again!!!");
		/*
		 * System.setProperty(
		 * "webdriver.gecko.driver","C:\\Automation\\Selenium\\selenium Required Jars\\geckodriver.exe"
		 * );
		 * 
		 * WebDriver driver = new FirefoxDriver();
		 */

		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.o2.co.uk/shop/my-offers/mobile-broadband");
		System.out.println("Web url loaded ");

		// Click on the first tariff
		driver.findElement(By.xpath("//*[@id='qa-data']/a[@id='data_qa_Huawei_1']/form/button[1]")).click();
		
	}

}