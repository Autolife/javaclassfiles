package seleniumfirstclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Chromedriverexample {

	public static void main(String[] args) throws InterruptedException 
	{
System.out.println("Hello again!!!");
  // Optional, if not specified, WebDriver will search your path for chromedriver.
	System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
  driver.get("http://www.google.com/xhtml");
  Thread.sleep(5000);  // Let the user actually see something!
  WebElement searchBox = driver.findElement(By.name("q"));
  searchBox.sendKeys("ChromeDriver");
  searchBox.submit();
  Thread.sleep(5000);  // Let the user actually see something!
  //driver.quit();
}
}