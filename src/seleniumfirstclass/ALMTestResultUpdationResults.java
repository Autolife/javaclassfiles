package seleniumfirstclass;

import org.openqa.selenium.WebDriver;

import atu.alm.wrapper.ALMServiceWrapper;
import atu.alm.wrapper.IDefect;
import atu.alm.wrapper.ITestCase;
import atu.alm.wrapper.ITestCaseRun;
import atu.alm.wrapper.ITestSet;
import atu.alm.wrapper.enums.DefectPriority;
import atu.alm.wrapper.enums.DefectSeverity;
import atu.alm.wrapper.enums.DefectStatus;
import atu.alm.wrapper.enums.StatusAs;
import atu.alm.wrapper.exceptions.ALMServiceException;

@SuppressWarnings("unused")
public class ALMTestResultUpdationResults {
	// static WebDriver driver;

	public static void main(String[] args) throws ALMServiceException {
		createTestCaseRunAndTestCaseExecutionSteps();
	//	createDefect();
	   // createAttachmentForTestSet();
		System.out.println("Done!!");
		//driver.quit();
	}

	/*
	 * Example For Creating an Attachment for a TestSet
	 */
	
/*	public static void createAttachmentForTestSet() throws ALMServiceException {
		 ALMServiceWrapper wrapper = new ALMServiceWrapper(
                 "https://almo2uk.saas.hp.com/qcbin/");
         wrapper.connect("vinudeep.malalur", "April1985*", "DEFAULT_TELEFONICA_UK", "O_HS");
		ITestSet testSet = wrapper.getTestSet(
				"SampleTestSetFolder\\SubTestSetFolder1", "TestSet3", 2503);
		wrapper.newAttachment("C:\\Automation\\Data.xlsx", "My Attachment Description",
				testSet);
		wrapper.close();

	}

	/*
	 * Example For Creating a new Defect and Attaching a File
	 */
/*	public static void createDefect() throws ALMServiceException {
		  ALMServiceWrapper wrapper = new ALMServiceWrapper(
                  "https://almo2uk.saas.hp.com/qcbin/");

		wrapper.connect("vinudeep.malalur", "April1985*", "DEFAULT_TELEFONICA_UK", "O_HS");
		IDefect defect = wrapper.newDefect();
		defect.isReproducible(true);
		defect.setAssignedTo("vinudeep.malalur");
		defect.setDetectedBy("vinudeep.malalur");
		defect.setDescription("Checking ALM connectivity for selenium tests");
		defect.setDetectionDate("09/04/2017");
		defect.setPriority(DefectPriority.LOW);
		defect.setSeverity(DefectSeverity.LOW);
		defect.setStatus(DefectStatus.OPEN);
		defect.setSummary("My Defect/Bug Summary");
		System.out.println(defect.getDefectID());
		wrapper.newAttachment("C:\\Automation\\Data.xlsx", "My Attachment Description",
				defect);
		defect.save();
		wrapper.close();
	}

	*/
	public static void createTestCaseRunAndTestCaseExecutionSteps()
			throws ALMServiceException {
		 ALMServiceWrapper wrapper = new ALMServiceWrapper(
                 "https://almo2uk.saas.hp.com/qcbin/");
     
     
     wrapper.connect("vinudeep.malalur", "July2017*", "DEFAULT_TELEFONICA_UK", "O_HS_NEW");
       
		// Update Test Case Result and Attach a File

        ITestCase loginTest = wrapper.updateResult("SampleTestSetFolder\\SubTestSetFolder1",
                "TestSet3", 2502, "Login", StatusAs.PASSED);
		wrapper.newAttachment("C:\\Automation\\Data.xlsx", "My Attachment Description",
				loginTest);
		// Update Test Case Result and Attach a File
		ITestCase logoutTest = wrapper.updateResult
				("SampleTestSetFolder\\SubTestSetFolder1", "TestSet3", 2502,
				"Logout", StatusAs.NOT_COMPLETED);
		wrapper.newAttachment("C:\\Automation\\Data.xlsx", "My Attachment Description",
				logoutTest);

	// Create a new Run, Add the Steps for this Run and Attach a File
		ITestCaseRun loginRun = wrapper.createNewRun(loginTest, "Run 1",
				StatusAs.PASSED);
		//wrapper.addStep(run, stepName, as, description, expected, actual);
		wrapper.addStep(loginRun, "Enter username", StatusAs.PASSED,
				"Enters username", "enter", "enter");
		wrapper.addStep(loginRun, "Enter password", StatusAs.PASSED,
				"Enters password", "enter", "enter");
		wrapper.addStep(loginRun, "Click Login", StatusAs.PASSED,
				"user clicks on login", "", "");
		wrapper.newAttachment("C:\\Automation\\Data.xlsx", "My Attachment Description",
				loginRun);

		// Create a new Run, Add the Steps for this Run and Attach a File
		ITestCaseRun logoutRun = wrapper.createNewRun(logoutTest, "Run 2",
				StatusAs.PASSED);
		wrapper.addStep(logoutRun, "Enter username", StatusAs.PASSED,
				"Enters username", "enter", "enter");
		wrapper.addStep(logoutRun, "Enter password", StatusAs.PASSED,
				"Enters password", "enter", "enter");
		wrapper.addStep(logoutRun, "Click Logout", StatusAs.PASSED,
				"Logout is done", "", "");
		wrapper.newAttachment("C:\\Automation\\Data.xlsx", "My Attachment Description",
				logoutRun); 
		wrapper.close();
		    
}
    

}